const axios = require('axios')

const weatherUrl = 'https://goweather.herokuapp.com/weather/'

class Weather{
    /**
     * Describe attributes: temperature, wind, date
     */
    constructor() {
        this.temperature = ''
        this.wind = ''
        this.date = Date.now()
        this.description=''
    }

    async setWeather(cityName){
        await axios.get(weatherUrl + cityName).then((response) => {
            this.temperature = response.data.temperature
            this.wind = response.data.wind
        }).catch((error) => {return error.message})
    }

    async setForecast(cityName){
        await axios.get(weatherUrl + cityName).then((response) => {
            this.description = response.data.description
            this.temperature = response.data.temperature
        }).catch((error) => {return error.message})
    }

}



module.exports = {Weather}